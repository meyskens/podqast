"""
test the queue
"""

import sys
sys.path.append("../python")

from podcast.queue import QueueFactory
from podcast.factory import Factory
from podcast.queue import Queue
from podcast.podcast import Podcast
from podcast.podpost import Podpost
import podcast

def test_create_queue():
    """
    def if a queue is created
    """

    q1 = QueueFactory().get_queue()
    assert type(q1) == Queue
    
    q2 = QueueFactory().get_queue()
    assert q1 == q2

def test_queue_save():
    """
    does the queue save itself?
    """

    q = QueueFactory().get_queue()
    q.save()

def test_insert_top():
    """
    """

    q = QueueFactory().get_queue()
    l1 = len(q.podposts)
    p = Podcast('https://freakshow.fm/feed/opus/')
    e = p.get_first_entry()
    e2 = p.get_last_entry()

    e.save()
    q.insert_top(e.id)
    assert q.podposts[0] == e.id

    e2.save()
    q.insert_top(e2.id)

    assert q.podposts[0] == e2.id
    assert q.podposts[1] == e.id

    q.save()

def test_get_podposts():
    """
    Test listing of podposts
    """

    q = QueueFactory().get_queue()
    count = 0
    for post in q.get_podposts():
        assert type(post) == str
        assert type(Factory().get_podpost(post)) == Podpost
        count += 1

    assert count == 2
