"""
test the archive
"""

import sys
sys.path.append("../python")

from podcast.archive import ArchiveFactory, Archive
from podcast.factory import Factory
from podcast.podcast import Podcast
from podcast.podpost import Podpost
import podcast

def test_create_archive():
    """
    test if archive is created
    """

    a1 = ArchiveFactory().get_archive()
    assert type(a1) == Archive

    a2 = ArchiveFactory().get_archive()
    assert a1 == a2

def test_archive_save():
    """
    does the archive save itself?
    """

    a = ArchiveFactory().get_archive()
    a.save()

def test_insert():
    """
    """
    
    a = ArchiveFactory().get_archive()
    p = Podcast('https://freakshow.fm/feed/opus/')
    e = p.get_first_entry()
    e.save()
    a.insert(e.id)
    
    e2 = p.get_last_entry()
    e2.save()
    a.insert(e2.id)

    assert e.id in a.podposts
    assert e2.id in a.podposts

def test_get_archives():
    """
    Test listing of podposts
    """

    a = ArchiveFactory().get_archive()
    count = 0
    for post in a.get_podposts():
        assert type(post) == str
        assert type(Factory().get_podpost(post)) == Podpost
        count += 1

    assert count == 2
