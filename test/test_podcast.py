"""
Test Podcast functions
"""
import sys
sys.path.append("../python")

from podcast.podcast import Podcast
from podcast.podpost import Podpost

def test_feed_entry():
    """
    Get a feed entry
    """

    for e in ['https://freakshow.fm/feed/opus',
              'http://feeds.twit.tv/sn.xml',
              'http://www.bbc.co.uk/programmes/p02nrspf/episodes/downloads.rss']:
        p = Podcast(e)
        e = p.get_first_entry()
        if e != None:
            assert type(e) == Podpost

            d = e.get_data()
            assert 'id' in d.keys()
            assert 'logo_url' in d.keys()
            assert 'description' in d.keys()
            print(d['id'], d['title'])
            print(d['logo_url'], d['description'])
            assert len(d['id']) > 0
            assert type(d['id']) == str
            assert len(d['logo_url']) > 0 or d['logo_url'] == ""
            assert type(d['logo_url']) == str
            e2 = p.get_entry(d['id'])
            assert e == e2
