import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: externalhandler

    signal createExternalList(var data)
    signal externalPodList(var data)
    signal externalUpdated()
    signal objectLoaded()
    signal audioInfo(string info)

    Component.onCompleted: {
        setHandler("createExternalList", createExternalList)
        setHandler("externalPodList", externalPodList)
        setHandler("externalUpdated", externalUpdated)
        setHandler("objectLoaded", objectLoaded)
        setHandler("audioInfo", audioInfo)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('ExternalHandler', function () {
            console.log('ExternalHandler is now imported')
        })
    }

    function getExternalEntries() {
        // call("ExternalHandler.externalhandler.getexternalposts", function() {});
        call("ExternalHandler.get_external_posts", function() {});
    }

//    function getExternalPodData() {
//        call("ExternalHandler.externalhandler.getexternalpoddata", function() {});
//        // call("ExternalHandler.get_external_pod_data", function() {});
//    }
    function waitNew() {
        call("ExternalHandler.externalhandler.waitnew", function() {});
        // call("ExternalHandler.wait_new", function() {});
    }
    function checkNew() {
        call("ExternalHandler.check_new", function() {});
    }
    function getAudioData(audio_file) {
        call("ExternalHandler.get_audio_data", [audio_file], function() {})
    }
}
